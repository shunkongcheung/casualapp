from django.contrib import admin

# Register your models here.
from .gnl_accesslog.admin import *
from .gnl_lookup.admin import *
from .gnl_media.admin import *
from .gnl_syslog.admin import *
