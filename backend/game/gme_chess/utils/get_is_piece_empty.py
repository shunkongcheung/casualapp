from .prefixes import CHS_EMPTY


def get_is_piece_empty(piece):
    return piece == CHS_EMPTY
