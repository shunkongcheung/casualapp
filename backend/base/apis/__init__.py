from .create_apis import MyCreateAPIView
from .list_apis import MyListAPIView
from .object_apis import MyObjectAPIView
