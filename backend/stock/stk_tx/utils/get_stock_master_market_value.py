
def get_stock_master_market_value(stock_master):
    return stock_master.market_price * stock_master.share_count
