# Generated by Django 2.2.5 on 2019-10-23 15:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('stock', '0023_auto_20191020_0938'),
    ]

    operations = [
        migrations.AddField(
            model_name='stockprofile',
            name='dividend_proportion_cost',
            field=models.FloatField(default=0.0),
        ),
        migrations.AddField(
            model_name='stockprofile',
            name='dividend_static_cost',
            field=models.FloatField(default=0.0),
        ),
    ]
