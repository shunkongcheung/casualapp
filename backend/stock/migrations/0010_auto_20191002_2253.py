# Generated by Django 2.2.5 on 2019-10-02 14:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('stock', '0009_ccassparticipantdetail'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ccassparticipantdetail',
            name='participant_master',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='participant_details', to='stock.CCASSParticipantMaster'),
        ),
    ]
