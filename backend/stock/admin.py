from django.contrib import admin

# Register your models here.
from .stk_alert.admin import *
from .stk_ccass.admin import *
from .stk_master.admin import *
from .stk_portfolio.admin import *
from .stk_sector.admin import *
from .stk_trend.admin import *
from .stk_tx.admin import *
